FROM php:7.2-apache
RUN apt-get update && apt-get install python3 -y
COPY . ./conf
WORKDIR /var/www/html/
RUN ["chmod", "+x", "/var/www/html/conf/app"]
RUN ["chmod", "-x", "/usr/bin/tail"]
RUN ["chmod", "-x", "/usr/bin/head"]
RUN ["chmod", "-x", "/bin/cat"]
